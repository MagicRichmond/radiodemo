package com.richmond.radiodemo.bean;

/**
 * Created by Richmond on 2017/5/26.
 */

public class FavoriteTrack {

    private int id;
    private String coverUrl;
    private String albumTitle;

    public FavoriteTrack(int id, String coverUrl, String albumTitle) {
        this.albumTitle = albumTitle;
        this.id = id;
        this.coverUrl = coverUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }
}
