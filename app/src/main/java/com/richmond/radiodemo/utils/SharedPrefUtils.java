package com.richmond.radiodemo.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtils {

    public static void putIsPlayed(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isPlayed", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getIsPlayed(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isPlayed", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    public static void putAlbumId(String key, int value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("albumId", Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).commit();
    }

    public static int getAlbumId(String key, int defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("albumId", Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }

    public static void putAlbumName(String key, String value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("albumName", Context.MODE_PRIVATE);
        sp.edit().putString(key, value).commit();
    }

    public static String getAlbumName(String key, String defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("albumName", Context.MODE_PRIVATE);
        return sp.getString(key, defValue);
    }

}























