package com.richmond.radiodemo.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ximalaya.ting.android.opensdk.player.XmPlayerManager;

/**
 * 播放状态广播
 * <p>
 * Created by Richmond on 2017/5/17.
 */
public class PlayerControlReceiver extends BroadcastReceiver {

    public static final String ACTION_PLAYER_CONTROL_PAUSE = "ACTION_PLAYER_CONTROL_PAUSE";
    public static final String ACTION_PLAYER_CONTROL_PRE = "ACTION_PLAYER_CONTROL_PRE";
    public static final String ACTION_PLAYER_CONTROL_NEXT = "ACTION_PLAYER_CONTROL_NEXT";

    @Override
    public void onReceive(Context context, Intent intent) {
        XmPlayerManager manager = XmPlayerManager.getInstance(context);
        String action = intent.getAction();
        if (ACTION_PLAYER_CONTROL_PAUSE.equals(action)) {
            System.out.println("111");
            if (manager.isPlaying()) {
                manager.pause();
            } else {
                manager.play();
            }
        } else if (ACTION_PLAYER_CONTROL_NEXT.equals(action)) {
            System.out.println("222");
            manager.playNext();
        } else if (ACTION_PLAYER_CONTROL_PRE.equals(action)) {
            System.out.println("333");
            manager.playPre();
        }
    }

}
