package com.richmond.radiodemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.richmond.radiodemo.App;
import com.richmond.radiodemo.R;
import com.richmond.radiodemo.adapter.PlayVpAdapter;
import com.richmond.radiodemo.bean.FavoriteTrack;
import com.richmond.radiodemo.utils.BitmapCache;
import com.richmond.radiodemo.utils.ListDataSave;
import com.richmond.radiodemo.utils.SharedPrefUtils;
import com.richmond.radiodemo.view.VerticalViewPager;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.track.Track;
import com.ximalaya.ting.android.opensdk.model.track.TrackList;
import com.ximalaya.ting.android.opensdk.player.XmPlayerManager;
import com.ximalaya.ting.android.player.XMediaPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.X;


public class PlayActivity extends Activity implements View.OnClickListener {

    private int mTrackId;
    private String mAlbumName;
    //    private ListView mListView;
    private List<Track> mTrackList = new ArrayList<>();
    private NetworkImageView mImageView;

    private RequestQueue mQueue;
    private ImageLoader mImageLoader;

    private Button mBtnPlayPause, mBtnNext, mBtnPrevious, mBtnSave;
    private TextView mTextView;
    private Button mBtnPlayMore;

    private VerticalViewPager mVerticalViewPager;
    private XmPlayerManager mXmPlayerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        mXmPlayerManager = XmPlayerManager.getInstance(this);

        initView();
        initData();
        initEvent();
    }

    private void initData() {
        mTrackId = getIntent().getIntExtra("albumId", 0);
        mAlbumName = getIntent().getStringExtra("albumName");

        mTextView.setText(mAlbumName);

        mQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(mQueue, new BitmapCache());
        mImageView.setErrorImageResId(R.mipmap.ic_launcher_round);
        mImageView.setDefaultImageResId(R.mipmap.ic_launcher);

        new Thread(new Runnable() {
            @Override
            public void run() {
                getTrackData();
            }
        }).start();
    }

    private void initEvent() {
        mBtnPlayMore.setOnClickListener(this);
        mBtnPlayPause.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mBtnPrevious.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
    }

    private void initView() {
        mBtnPlayMore = (Button) findViewById(R.id.btn_play_more);
        mBtnNext = (Button) findViewById(R.id.btn_next_radio);
        mBtnPrevious = (Button) findViewById(R.id.btn_previous_radio);
        mBtnPlayPause = (Button) findViewById(R.id.btn_play_pause);
        mBtnSave = (Button) findViewById(R.id.btn_save_radio);
        mImageView = (NetworkImageView) findViewById(R.id.iv_play_album);
//        mListView = (ListView) findViewById(R.id.lv_track_play);
        mTextView = (TextView) findViewById(R.id.tv_play_track);
        mVerticalViewPager = (VerticalViewPager) findViewById(R.id.vp_play);
    }

    private void getTrackData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(DTransferConstants.ALBUM_ID, String.valueOf(mTrackId));
        map.put(DTransferConstants.SORT, "asc");
//        map.put(DTransferConstants.PAGE, mPage);
        map.put(DTransferConstants.PAGE_SIZE, String.valueOf(100));
        CommonRequest.getTracks(map, new IDataCallBack<TrackList>() {

            @Override
            public void onSuccess(TrackList trackList) {
                System.out.println(trackList);
                mTrackList = trackList.getTracks();
//                mListView.setAdapter(new ListPlayAdapter(PlayActivity.this, mTrackList));
                mImageView.setImageUrl(mTrackList.get(0).getCoverUrlLarge(), mImageLoader);
                mVerticalViewPager.setAdapter(new PlayVpAdapter(PlayActivity.this, mTrackList, mVerticalViewPager));

                if (mXmPlayerManager.isConnected()) {
                    if (!(mTrackId == SharedPrefUtils.getAlbumId("albumId", 0, PlayActivity.this))) {
                        mXmPlayerManager.stop();
                        mXmPlayerManager.playList(mTrackList, 0);
                        SharedPrefUtils.putAlbumId("albumId", mTrackId, PlayActivity.this);
                        SharedPrefUtils.putAlbumName("albumName", mAlbumName, PlayActivity.this);
                    }
                } else {
                    Toast.makeText(PlayActivity.this, "声音连接失败", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(int i, String s) {
                Toast.makeText(PlayActivity.this, "分类：专辑声音列表获取失败", Toast.LENGTH_LONG).show();
                Log.d(App.TAG, " " + s);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_radio:
                List<FavoriteTrack> list = new ArrayList<>();
                ListDataSave listDataSave = new ListDataSave(this, "radioFavorite");
                if (null != list) {
                    list = listDataSave.getDataList("radioFavorite");
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getId() == mTrackId) {
                            list.remove(i);
                            listDataSave.setDataList("radioFavorite", list);
                            return;
                        }
                    }
                    list.add(new FavoriteTrack(mTrackId, mTrackList.get(0).getCoverUrlMiddle(), mAlbumName));
                    listDataSave.setDataList("radioFavorite", list);
                    return;
                } else {
                    list.add(new FavoriteTrack(mTrackId, mTrackList.get(0).getCoverUrlMiddle(), mAlbumName));
                    listDataSave.setDataList("radioFavorite", list);
                    return;
                }
            case R.id.btn_play_pause:
                if (mXmPlayerManager.isPlaying()) {
                    mXmPlayerManager.pause();
                } else {
                    mXmPlayerManager.play();
                }
                break;
            case R.id.btn_previous_radio:
                XmPlayerManager.getInstance(this).playPre();
                break;
            case R.id.btn_next_radio:
                XmPlayerManager.getInstance(this).playNext();
                break;
            case R.id.btn_play_more:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            default:
                break;
        }
    }
}
