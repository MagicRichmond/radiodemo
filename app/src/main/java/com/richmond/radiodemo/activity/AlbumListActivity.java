package com.richmond.radiodemo.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.richmond.radiodemo.App;
import com.richmond.radiodemo.R;
import com.richmond.radiodemo.adapter.RvAlbumAdapter;
import com.richmond.radiodemo.view.GridSpacingItemDecoration;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.album.AlbumList;

import java.util.HashMap;
import java.util.Map;

import static com.richmond.radiodemo.App.getContext;

public class AlbumListActivity extends Activity {

    private RecyclerView mRecyclerView;
    public static final int COLUMN_COUNT = 5;
    private int mCategoryId;
    private String mCategoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list);

        mCategoryId = getIntent().getIntExtra("categoryId", 12);
        mCategoryName = getIntent().getStringExtra("categoryName");

        TextView textView = (TextView) findViewById(R.id.tv_recyclerview_title);
        textView.setText(mCategoryName);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_album_list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, COLUMN_COUNT, LinearLayoutManager.VERTICAL, false));
        int itemHoritonzalSpace = 32;
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(COLUMN_COUNT, itemHoritonzalSpace, false));

        initData();
    }

    private void initData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                getAlbumListData(mCategoryId);
            }
        }).start();
    }


    public void onBack(View view) {
        finish();
    }


    // 分类下专辑列表数据    （经典或播放最多、20个）
    private void getAlbumListData(int categoryId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(DTransferConstants.CATEGORY_ID, String.valueOf(categoryId));
        map.put(DTransferConstants.CALC_DIMENSION, "3");
//                map.put(DTransferConstants.TAG_NAME, "");
        map.put(DTransferConstants.PAGE, "1");
        map.put(DTransferConstants.PAGE_SIZE, "20");
        CommonRequest.getAlbumList(map, new IDataCallBack<AlbumList>() {

            @Override
            public void onSuccess(AlbumList albumList) {
                Log.d(App.TAG, " getAlbumList Success");
                mRecyclerView.setAdapter(new RvAlbumAdapter(AlbumListActivity.this, albumList.getAlbums()));
            }

            @Override
            public void onError(int i, String s) {
                Toast.makeText(getContext(), "分类下专辑列表数据获取失败", Toast.LENGTH_LONG).show();
                Log.d(App.TAG, " i = " + i + "  s = " + s);
            }
        });
    }
}
