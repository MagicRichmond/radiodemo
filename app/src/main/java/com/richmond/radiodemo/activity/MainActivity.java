package com.richmond.radiodemo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.richmond.radiodemo.R;
import com.richmond.radiodemo.fragment.RadioBaseFragment;
import com.richmond.radiodemo.fragment.RadioCategoryFragment;
import com.richmond.radiodemo.fragment.RadioFavoritesFragment;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.PlayableModel;
import com.ximalaya.ting.android.opensdk.model.album.AlbumList;
import com.ximalaya.ting.android.opensdk.player.XmPlayerManager;
import com.ximalaya.ting.android.opensdk.player.service.IXmPlayerStatusListener;
import com.ximalaya.ting.android.opensdk.player.service.XmPlayerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.id;
import static android.R.id.message;

public class MainActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private Button mBtnFavorites, mBtnCategory;
    private ViewPager mViewPager;
    private MyFragmentPagerAdapter mAdapter;

    private List<RadioBaseFragment> mFragmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();
        initView();
        initEvent();
    }

    private void initData() {

        RadioCategoryFragment categoryFragment = new RadioCategoryFragment();
        RadioFavoritesFragment favoritesFragment = new RadioFavoritesFragment();
        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        transaction.add(R.id.fl_content, categoryFragment);
//        transaction.add(R.id.fl_content, favoritesFragment);
//        transaction.commit();

        mFragmentList = new ArrayList<>();
        mFragmentList.add(categoryFragment);
        mFragmentList.add(favoritesFragment);
        mAdapter = new MyFragmentPagerAdapter(fm);
    }

    private void initView() {
        mViewPager = (ViewPager) findViewById(R.id.vp_radio);
        mBtnCategory = (Button) findViewById(R.id.btn_radio_category);
        mBtnFavorites = (Button) findViewById(R.id.btn_radio_favorites);
        mViewPager.setAdapter(mAdapter);
    }

    private void initEvent() {
        mBtnFavorites.setOnClickListener(this);
        mBtnCategory.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_radio_favorites:
                if (mViewPager.getCurrentItem() != 1)
                    mViewPager.setCurrentItem(1);
                break;
            case R.id.btn_radio_category:
                if (mViewPager.getCurrentItem() != 0)
                    mViewPager.setCurrentItem(0);
                break;

            default:
                break;
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    class MyFragmentPagerAdapter extends FragmentPagerAdapter {


        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }
}

