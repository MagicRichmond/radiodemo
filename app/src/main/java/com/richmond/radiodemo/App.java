package com.richmond.radiodemo;

import android.app.Application;
import android.content.Context;

/**
 * Created by Richmond on 2017/5/17.
 */

public class App extends Application {

    public static final String TAG = "Radio";

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    public static Context getContext() {
        return sContext;
    }
}
