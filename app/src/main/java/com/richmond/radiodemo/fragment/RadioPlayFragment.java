package com.richmond.radiodemo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.richmond.radiodemo.App;
import com.richmond.radiodemo.R;
import com.richmond.radiodemo.activity.MainActivity;
import com.richmond.radiodemo.adapter.PlayVpAdapter;
import com.richmond.radiodemo.bean.FavoriteTrack;
import com.richmond.radiodemo.utils.BitmapCache;
import com.richmond.radiodemo.utils.ListDataSave;
import com.richmond.radiodemo.utils.SharedPrefUtils;
import com.richmond.radiodemo.view.VerticalViewPager;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.PlayableModel;
import com.ximalaya.ting.android.opensdk.model.track.Track;
import com.ximalaya.ting.android.opensdk.model.track.TrackList;
import com.ximalaya.ting.android.opensdk.player.XmPlayerManager;
import com.ximalaya.ting.android.opensdk.player.service.IXmPlayerStatusListener;
import com.ximalaya.ting.android.opensdk.player.service.XmPlayListControl;
import com.ximalaya.ting.android.opensdk.player.service.XmPlayerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Richmond on 2017/5/17.
 */

public class RadioPlayFragment extends RadioBaseFragment implements View.OnClickListener, IXmPlayerStatusListener {

    private final String APP_SECRET = "de61d3b980608201537bfc3794ee4d63";
    //  SDK，播放器初始化
    private CommonRequest mCommonRequest;
    private XmPlayerManager mXmPlayerManager;

    private View mFragmentView;
    private int mTrackId;
    private String mAlbumName;
    //    private ListView mListView;
    private List<Track> mTrackList = new ArrayList<>();
    private NetworkImageView mImageView;

    private RequestQueue mQueue;
    private ImageLoader mImageLoader;

    private Button mBtnPlayPause, mBtnNext, mBtnPrevious, mBtnSave;
    private TextView mTextView;
    private Button mBtnPlayMore;

    private VerticalViewPager mVerticalViewPager;

    public RadioPlayFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.activity_play, null);

        return mFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCommonRequest = CommonRequest.getInstanse();
        mCommonRequest.init(getContext(), APP_SECRET);
        mXmPlayerManager = XmPlayerManager.getInstance(getContext());
        mXmPlayerManager.init();
        mXmPlayerManager.setPlayMode(XmPlayListControl.PlayMode.PLAY_MODEL_LIST_LOOP);
        mXmPlayerManager.addPlayerStatusListener(this);

        initView();
        initEvent();
    }

    @Override
    public void onResume() {
        super.onResume();

        initData();
    }

    private void initData() {
        mTrackId = SharedPrefUtils.getAlbumId("albumId", 0, getContext());
        mAlbumName = SharedPrefUtils.getAlbumName("albumName", "微电台", getContext());

        mTextView.setText(mAlbumName);

        mQueue = Volley.newRequestQueue(getContext());
        mImageLoader = new ImageLoader(mQueue, new BitmapCache());
        mImageView.setErrorImageResId(R.mipmap.ic_launcher_round);
        mImageView.setDefaultImageResId(R.mipmap.ic_launcher);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mTrackId != 0)
                    getTrackData();
            }
        }).start();
    }

    private void initEvent() {
        mBtnPlayMore.setOnClickListener(this);
        mBtnPlayPause.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mBtnPrevious.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
    }

    private void initView() {
        mBtnPlayMore = (Button) mFragmentView.findViewById(R.id.btn_play_more);
        mBtnNext = (Button) mFragmentView.findViewById(R.id.btn_next_radio);
        mBtnPrevious = (Button) mFragmentView.findViewById(R.id.btn_previous_radio);
        mBtnPlayPause = (Button) mFragmentView.findViewById(R.id.btn_play_pause);
        mBtnSave = (Button) mFragmentView.findViewById(R.id.btn_save_radio);
        mImageView = (NetworkImageView) mFragmentView.findViewById(R.id.iv_play_album);
//        mListView = (ListView) findViewById(R.id.lv_track_play);
        mTextView = (TextView) mFragmentView.findViewById(R.id.tv_play_track);
        mVerticalViewPager = (VerticalViewPager) mFragmentView.findViewById(R.id.vp_play);
    }

    private void getTrackData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(DTransferConstants.ALBUM_ID, String.valueOf(mTrackId));
        map.put(DTransferConstants.SORT, "asc");
//        map.put(DTransferConstants.PAGE, mPage);
        map.put(DTransferConstants.PAGE_SIZE, String.valueOf(100));
        CommonRequest.getTracks(map, new IDataCallBack<TrackList>() {

            @Override
            public void onSuccess(TrackList trackList) {
                System.out.println(trackList);
                mTrackList = trackList.getTracks();
//                mListView.setAdapter(new ListPlayAdapter(PlayActivity.this, mTrackList));
                mImageView.setImageUrl(mTrackList.get(0).getCoverUrlLarge(), mImageLoader);
                mVerticalViewPager.setAdapter(new PlayVpAdapter(getContext(), mTrackList, mVerticalViewPager));
                if (!mXmPlayerManager.isConnected()) {
                    System.out.println("播放连接失败！！");
                } else {
                    if (!mXmPlayerManager.isPlaying()) {
                        System.out.println("=== " + mTrackList);
                        mXmPlayerManager.playList(mTrackList, 0);
                    }
                }
            }

            @Override
            public void onError(int i, String s) {
//                Toast.makeText(getActivity(), "分类：专辑声音列表获取失败", Toast.LENGTH_LONG).show();
                Log.d(App.TAG, " " + s);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_radio:
                List<FavoriteTrack> list = new ArrayList<>();
                ListDataSave listDataSave = new ListDataSave(getContext(), "radioFavorite");
                if (null != list) {
                    list = listDataSave.getDataList("radioFavorite");
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getId() == mTrackId) {
                            list.remove(i);
                            listDataSave.setDataList("radioFavorite", list);
                            return;
                        }
                    }
                    list.add(new FavoriteTrack(mTrackId, mTrackList.get(0).getCoverUrlMiddle(), mAlbumName));
                    listDataSave.setDataList("radioFavorite", list);
                    return;
                } else {
                    list.add(new FavoriteTrack(mTrackId, mTrackList.get(0).getCoverUrlMiddle(), mAlbumName));
                    listDataSave.setDataList("radioFavorite", list);
                    return;
                }
            case R.id.btn_play_pause:
//                    mXmPlayerManager.playList(mTrackList, 0);
                if (mXmPlayerManager.isPlaying()) {
                    mXmPlayerManager.pause();
                } else {
                    mXmPlayerManager.play();
                }
                break;
            case R.id.btn_previous_radio:
                mXmPlayerManager.playPre();
                break;
            case R.id.btn_next_radio:
                mXmPlayerManager.playNext();
                break;
            case R.id.btn_play_more:
                startActivity(new Intent(getContext(), MainActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mXmPlayerManager.removePlayerStatusListener(this);
    }

    @Override
    public void onPlayStart() {
        System.out.println("playStart");
    }

    @Override
    public void onPlayPause() {
        System.out.println("playPause");
    }

    @Override
    public void onPlayStop() {
        System.out.println("playStop");
    }

    @Override
    public void onSoundPlayComplete() {
        System.out.println("onSoundPlayComplete");
    }

    @Override
    public void onSoundPrepared() {
        System.out.println("onSoundPlayPrepared");
    }

    @Override
    public void onSoundSwitch(PlayableModel playableModel, PlayableModel playableModel1) {
        System.out.println("onSoundSwitch");
    }

    @Override
    public void onBufferingStart() {
        System.out.println("onBufferingStart");
    }

    @Override
    public void onBufferingStop() {
        System.out.println("onBufferingStop");
    }

    @Override
    public void onBufferProgress(int i) {
//        System.out.println("onBufferProgress i = " + i);
    }

    @Override
    public void onPlayProgress(int i, int i1) {
//        System.out.println("onBufferProgress i = " + i + "  i1 = " + i1);
    }

    @Override
    public boolean onError(XmPlayerException e) {
        System.out.println("onError : " + e);
        return false;
    }
}
