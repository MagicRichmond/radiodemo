package com.richmond.radiodemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.richmond.radiodemo.App;
import com.richmond.radiodemo.R;
import com.richmond.radiodemo.adapter.ListCategoryAdapter;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.album.Album;
import com.ximalaya.ting.android.opensdk.model.album.AlbumList;
import com.ximalaya.ting.android.opensdk.model.category.Category;
import com.ximalaya.ting.android.opensdk.model.category.CategoryList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Richmond on 2017/5/17.
 */

public class RadioCategoryFragment extends RadioBaseFragment {

    private View mFragmentView;
    private ListView mListView;

    private List<Category> mCategoryList = new ArrayList<Category>();
    private Map<Integer, List<Album>> mMap = new HashMap<Integer, List<Album>>();
    private ListCategoryAdapter mCategoryAdapter;

    public RadioCategoryFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_radio_category, null);
        mListView = (ListView) mFragmentView.findViewById(R.id.lv_category);

        return mFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
                getCategoryData();
            }
        }).start();
    }

    //  分类数据
    private void getCategoryData() {
        Map<String, String> map = new HashMap<String, String>();
        CommonRequest.getCategories(map, new IDataCallBack<CategoryList>() {
            @Override
            public void onSuccess(CategoryList object) {
                if (null != object.getCategories() && null != object) {
                    Log.d(App.TAG, " getCategories Success!");
                    initXuiCategoryData(object);
                }
            }

            @Override
            public void onError(int code, String message) {
                Toast.makeText(getActivity(), "分类数据获取失败", Toast.LENGTH_LONG).show();
                Log.d(App.TAG, " code = " + code + "  message = " + message);
            }
        });
    }

    //  初始化数据（相声评书：12  脱口秀：28 历史：9 汽车：21 资讯：1 商业财经：8 IT科技：18 旅游：22 时尚生活：31）
    private void initXuiCategoryData(CategoryList categoryList) {
        List<Integer> integers = new ArrayList<>();
        integers.add(12);
        integers.add(28);
        integers.add(9);
        integers.add(21);
        integers.add(1);
        integers.add(8);
        integers.add(18);
        integers.add(22);
        integers.add(31);

        List<Category> list = categoryList.getCategories();
        for (int i = 0; i < list.size(); i++) {
            if (integers.contains((int) list.get(i).getId()))
                mCategoryList.add(list.get(i));
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < mCategoryList.size(); i++) {
                    getAlbumListData((int) mCategoryList.get(i).getId());
                }
            }
        }).start();
    }

    // 分类下专辑列表数据    （经典或播放最多、20个）
    private void getAlbumListData(final int categoryId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(DTransferConstants.CATEGORY_ID, String.valueOf(categoryId));
        map.put(DTransferConstants.CALC_DIMENSION, "3");
//                map.put(DTransferConstants.TAG_NAME, "");
        map.put(DTransferConstants.PAGE, "1");
        map.put(DTransferConstants.PAGE_SIZE, "20");
        CommonRequest.getAlbumList(map, new IDataCallBack<AlbumList>() {

            @Override
            public void onSuccess(AlbumList albumList) {
                Log.d(App.TAG, " getAlbumList Success");
                mMap.put(categoryId, albumList.getAlbums());

                if (mMap.size() == mCategoryList.size()) {
                    mListView.setAdapter(new ListCategoryAdapter(getContext(), mCategoryList, mMap));
                }
            }

            @Override
            public void onError(int i, String s) {
                Toast.makeText(getContext(), "分类下专辑列表数据获取失败", Toast.LENGTH_LONG).show();
                Log.d(App.TAG, " i = " + i + "  s = " + s);
            }
        });
    }

}
