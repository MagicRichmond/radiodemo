package com.richmond.radiodemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richmond.radiodemo.R;
import com.richmond.radiodemo.adapter.RvFavoriteAdapter;
import com.richmond.radiodemo.bean.FavoriteTrack;
import com.richmond.radiodemo.utils.ListDataSave;
import com.richmond.radiodemo.view.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import static com.richmond.radiodemo.activity.AlbumListActivity.COLUMN_COUNT;

/**
 * Created by Richmond on 2017/5/17.
 */

public class RadioFavoritesFragment extends RadioBaseFragment {

    private View mFragmentView;
    private RecyclerView mRecyclerView;
    private List<FavoriteTrack> mFavoriteTrackList = new ArrayList<>();

    public RadioFavoritesFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.activity_album_list, null);
        mFragmentView.findViewById(R.id.rl_recyclerview_title).setVisibility(View.GONE);

        return mFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.rv_album_list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), COLUMN_COUNT, LinearLayoutManager
                .VERTICAL, false));
        int itemHoritonzalSpace = 32;
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(COLUMN_COUNT, itemHoritonzalSpace, false));

        List<FavoriteTrack> list = new ArrayList<>();
        ListDataSave listDataSave = new ListDataSave(getContext(), "radioFavorite");
        list = listDataSave.getDataList("radioFavorite");

        mRecyclerView.setAdapter(new RvFavoriteAdapter(getContext(), list));
    }

}
