package com.richmond.radiodemo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.richmond.radiodemo.R;
import com.richmond.radiodemo.activity.PlayActivity;
import com.richmond.radiodemo.utils.SharedPrefUtils;
import com.richmond.radiodemo.view.VerticalViewPager;
import com.ximalaya.ting.android.opensdk.model.track.Track;
import com.ximalaya.ting.android.opensdk.player.XmPlayerManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richmond on 2017/5/26.
 */

public class PlayVpAdapter extends PagerAdapter implements View.OnClickListener {

    private Context mContext;
    private List<Track> mTrackList = new ArrayList<>();
    private View mListView;
    private int pageCount;
    private static final int PER_PAGE_COUNT = 6;
    private boolean isDevided;
    private VerticalViewPager mViewPager;

    public PlayVpAdapter(Context context, List<Track> trackList, VerticalViewPager viewPager) {
        mContext = context;
        mTrackList = trackList;
        isDevided = trackList.size() % PER_PAGE_COUNT == 0;
        pageCount = (isDevided ? trackList.size() / PER_PAGE_COUNT : trackList.size() / PER_PAGE_COUNT + 1);
        mViewPager = viewPager;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mListView = LayoutInflater.from(mContext).inflate(R.layout.vp_play, null);
        RadioGroup linearLayout = (RadioGroup) mListView.findViewById(R.id.ll_play);
        if (isDevided) {
            for (int i = 0; i < PER_PAGE_COUNT; i++) {
                TextView textView = (TextView) linearLayout.getChildAt(i);
                textView.setText(mTrackList.get(position * PER_PAGE_COUNT + i).getTrackTitle());
                textView.setOnClickListener(this);
            }
        } else {
            if (position < pageCount - 1) {
                for (int i = 0; i < PER_PAGE_COUNT; i++) {
                    TextView textView = (TextView) linearLayout.getChildAt(i);
                    textView.setText(mTrackList.get(position * PER_PAGE_COUNT + i).getTrackTitle());
                    textView.setOnClickListener(this);
                }
            } else {
                for (int i = 0; i < mTrackList.size() % PER_PAGE_COUNT; i++) {
                    TextView textView = (TextView) linearLayout.getChildAt(i);
                    textView.setText(mTrackList.get(position * PER_PAGE_COUNT + i).getTrackTitle());
                    textView.setOnClickListener(this);
                }
            }
        }

        container.addView(mListView);

        return mListView;
    }

    @Override
    public int getCount() {
        return pageCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onClick(View v) {
        int clickPosition = 0;
        switch (v.getId()) {
            case R.id.first:
                clickPosition = 1;
                break;
            case R.id.second:
                clickPosition = 2;
                break;
            case R.id.third:
                clickPosition = 3;
                break;
            case R.id.fourth:
                clickPosition = 4;
                break;
            case R.id.fifth:
                clickPosition = 5;
                break;
            case R.id.sixth:
                clickPosition = 6;
                break;
            default:
                break;
        }
        int item = clickPosition + mViewPager.getCurrentItem();
        System.out.println("item = " + item);
        XmPlayerManager xmPlayerManager = XmPlayerManager.getInstance(mContext);

        if (xmPlayerManager.isConnected()) {
//            xmPlayerManager.stop();
            xmPlayerManager.playList(mTrackList, item);

            SharedPrefUtils.putAlbumId("albumId", (int) mTrackList.get(item).getAlbum().getAlbumId(), mContext);
            SharedPrefUtils.putAlbumName("albumName", mTrackList.get(item).getAlbum().getAlbumTitle(), mContext);
        } else {
            Toast.makeText(mContext, "声音连接失败", Toast.LENGTH_LONG).show();
        }
    }
}
