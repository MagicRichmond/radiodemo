package com.richmond.radiodemo.adapter;//package com.richmond.radiodemo.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//
//import com.richmond.radiodemo.R;
//import com.ximalaya.ting.android.opensdk.model.track.Track;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by gcf on 2016/8/24.
// */
//public class ListPlayAdapter extends BaseAdapter {
//
//    private List<Track> mTrackList = new ArrayList<Track>();
//    private Context mContext;
//    private LayoutInflater mLayoutInflater;
//
//    public ListPlayAdapter(Context context, List<Track> trackList) {
//        mLayoutInflater = LayoutInflater.from(context);
//        mContext = context;
//        mTrackList.clear();
//        mTrackList = trackList;
//    }
//
//    @Override
//    public int getCount() {
//        return mTrackList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mTrackList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        ViewHolder viewHolder = null;
//        if (convertView == null) {
//            viewHolder = new ViewHolder();
//            convertView = mLayoutInflater.inflate(R.layout.lv_play_item, null);
//            viewHolder.mTvTrackName = (TextView) convertView.findViewById(R.id.tv_play_item_name);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//
//        viewHolder.mTvTrackName.setText(mTrackList.get(position).getTrackTitle());
//        viewHolder.mTvTrackName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//
//        return convertView;
//    }
//
//    class ViewHolder {
//        public TextView mTvTrackName;
//    }
//}
