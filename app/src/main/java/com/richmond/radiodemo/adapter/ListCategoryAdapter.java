package com.richmond.radiodemo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.richmond.radiodemo.activity.AlbumListActivity;
import com.richmond.radiodemo.activity.MainActivity;
import com.richmond.radiodemo.activity.PlayActivity;
import com.richmond.radiodemo.utils.BitmapCache;
import com.richmond.radiodemo.utils.SharedPrefUtils;
import com.richmond.radiodemo.view.MyTextView;
import com.richmond.radiodemo.R;
import com.ximalaya.ting.android.opensdk.model.album.Album;
import com.ximalaya.ting.android.opensdk.model.category.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.id;
import static android.R.attr.start;
import static android.R.id.list;

/**
 * Created by gcf on 2016/8/24.
 */
public class ListCategoryAdapter extends BaseAdapter {

    private List<Category> mCategoryList = new ArrayList<Category>();
    private Map<Integer, List<Album>> mMap = new HashMap<Integer, List<Album>>();
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private RequestQueue mQueue;
    private ImageLoader mImageLoader;

    public ListCategoryAdapter(Context context, List<Category> categoryList, Map<Integer, List<Album>> map) {
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
        mCategoryList.clear();
        mCategoryList = categoryList;
        mMap = map;

        mQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mQueue, new BitmapCache());
    }

    @Override
    public int getCount() {
        return mMap.size();
    }

    @Override
    public Object getItem(int position) {
        return mMap.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.lv_category_item, null);
            viewHolder.mTvCategory = (TextView) convertView.findViewById(R.id.tv_category_name);
            viewHolder.mBtnCategoryMore = (Button) convertView.findViewById(R.id.btn_category_more);
            viewHolder.mRgLayoutName = (RadioGroup) convertView.findViewById(R.id.rg_category_list_name);
            viewHolder.mRgLayoutImage = (RadioGroup) convertView.findViewById(R.id.rg_category_list_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  大类
        String categoryName = mCategoryList.get(position).getCategoryName();
        viewHolder.mTvCategory.setText(categoryName);

        // 专辑列表
        initTagLayout(viewHolder, position);

        viewHolder.mBtnCategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AlbumListActivity.class);
                intent.putExtra("categoryId", (int) mCategoryList.get(position).getId());
                intent.putExtra("categoryName", mCategoryList.get(position).getCategoryName());
                mContext.startActivity(intent);
                Activity activity = (Activity) mContext;
                activity.finish();
            }
        });

        return convertView;
    }

    private void initTagLayout(ViewHolder viewHolder, final int position) {
        final List<Album> list = mMap.get((int) mCategoryList.get(position).getId());

        for (int i = 0; i < 6; i++) {
            MyTextView textView = (MyTextView) viewHolder.mRgLayoutName.getChildAt(i);
            textView.setText(list.get(i).getAlbumTitle());
            textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            textView.setSingleLine(true);
            textView.setMarqueeRepeatLimit(Integer.MAX_VALUE);

            final int a = i;
            NetworkImageView networkImageView = (NetworkImageView) viewHolder.mRgLayoutImage.getChildAt(i);
            networkImageView.setDefaultImageResId(R.mipmap.ic_launcher);
            networkImageView.setErrorImageResId(R.mipmap.ic_launcher_round);
            networkImageView.setImageUrl(list.get(i).getCoverUrlMiddle(),
                    mImageLoader);
            networkImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, PlayActivity.class);
                    intent.putExtra("albumName", list.get(a).getAlbumTitle());
                    intent.putExtra("albumId", (int) list.get(a).getId());
                    mContext.startActivity(intent);
                    SharedPrefUtils.putIsPlayed("isPlayed", true, mContext);
                    Activity activity = (Activity) mContext;
                    activity.finish();
                }
            });
        }
    }


    class ViewHolder {
        public TextView mTvCategory;
        public Button mBtnCategoryMore;
        public RadioGroup mRgLayoutName;
        public RadioGroup mRgLayoutImage;
    }
}
