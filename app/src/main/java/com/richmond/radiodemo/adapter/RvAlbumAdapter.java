package com.richmond.radiodemo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.richmond.radiodemo.R;
import com.richmond.radiodemo.activity.PlayActivity;
import com.richmond.radiodemo.utils.BitmapCache;
import com.richmond.radiodemo.utils.SharedPrefUtils;
import com.richmond.radiodemo.view.MyTextView;
import com.ximalaya.ting.android.opensdk.model.album.Album;

import java.util.List;


/**
 * Created by Richmond on 2017/3/27.
 */

public class RvAlbumAdapter extends RecyclerView.Adapter<RvAlbumAdapter.MyViewHolder> {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<Album> mDatas;

    private RequestQueue mQueue;
    private ImageLoader mImageLoader;

    public RvAlbumAdapter(Context context, List<Album> albumList) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mDatas = albumList;

        mQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mQueue, new BitmapCache());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(mInflater.inflate(
                R.layout.recyclerview_item, null, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.mTextView.setText(mDatas.get(position).getAlbumTitle());
//        holder.mImageView.setImageDrawable(mDatas.get(position));
        NetworkImageView networkImageView = holder.mImageView;
        networkImageView.setImageUrl(mDatas.get(position).getCoverUrlMiddle(),
                mImageLoader);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getLayoutPosition();
                int id = (int) mDatas.get(pos).getId();
                Intent intent = new Intent(mContext, PlayActivity.class);
                intent.putExtra("albumName", mDatas.get(pos).getAlbumTitle());
                intent.putExtra("albumId", id);
                mContext.startActivity(intent);
                SharedPrefUtils.putIsPlayed("isPlayed", true, mContext);
                Activity activity = (Activity) mContext;
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        MyTextView mTextView;
        NetworkImageView mImageView;

        public MyViewHolder(View view) {
            super(view);
            mTextView = (MyTextView) view.findViewById(R.id.tv_recyclerview_item);
            mTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            mTextView.setSingleLine(true);
            mTextView.setMarqueeRepeatLimit(Integer.MAX_VALUE);
            mImageView = (NetworkImageView) view.findViewById(R.id.iv_recyclerview_item);
            mImageView.setDefaultImageResId(R.mipmap.ic_launcher);
            mImageView.setErrorImageResId(R.mipmap.ic_launcher_round);
        }
    }

}
